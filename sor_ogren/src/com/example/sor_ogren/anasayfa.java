package com.example.sor_ogren;

import java.io.IOException;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class anasayfa extends Activity {
	
	
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.anasayfa);
		
		Button buton1 = (Button) findViewById(R.id.button1);
        Button buton2 = (Button) findViewById(R.id.button2);
        
        buton1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent("com.example.sor_ogren.EGITMENGIR"));
				
			}
		});
      
        buton2.setOnClickListener(new View.OnClickListener() {
	
        	@Override
        	public void onClick(View v) {
        		startActivity(new Intent("com.example.sorogren.OGRENCIGIR"));
		
	}
});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sor_ogren, menu);
		return true;
	}

}
